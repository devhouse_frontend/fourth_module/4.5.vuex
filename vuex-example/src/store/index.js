import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const cartModule = {
  namespaced: true,
  state: {
    cart: []
  },
  getters: {
    totalPrice(state) {
      return state.cart.reduce((prev, curr) => prev + curr.price, 0)
    }
  },
  mutations: {
    buy(state, product) {
      state.cart.push(product)
    },
  },
  actions: {
    async actualBuy({ commit }, product) {
      const response = await fetch('http://localhost:3000/products/' + product.id)
      const product_ = await response.json()
      commit('buy', product_)
    }
  },
}

export default new Vuex.Store({
  modules: {
    cartModule
  }
})
